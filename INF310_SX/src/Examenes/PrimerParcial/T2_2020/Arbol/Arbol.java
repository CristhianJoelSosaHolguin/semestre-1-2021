/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examenes.PrimerParcial.T2_2020.Arbol;

/**
 *
 * @author ronal
 */
public class Arbol {
    private Nodo Raiz;

    public Arbol() {
    }
    
    public Arbol(int x) {
        Raiz = new Nodo(x);
    }
    
    public void insertar(int x) {
        if(Raiz == null){
            Raiz = new Nodo(x);
            return;
        }
        Nodo aux1 = Raiz;
        Nodo aux2 = null;

        while(aux1 != null) {
            aux2 = aux1;
            if(x < aux1.getData()) {
                aux1 = aux1.getHI();
            } else if(x > aux1.getData()) {
                aux1 = aux1.getHD();
            } else {
                System.err.println("El elemento "+x+" ya éxiste en el árbol.");
                return;
            }
        }

        Nodo p = new Nodo(x);  
        if(x < aux2.getData()) {
            aux2.setHI(p);
        } else {
            aux2.setHD(p);
        }
    }
    
    public void inOrden() {
        inOrden(Raiz);
    }
    
    private void inOrden(Nodo p) {
        if(p == null)
            return;
        inOrden(p.getHI());
        System.out.print(p);
        inOrden(p.getHD());
    }
    
    public void delHoja(int papa, int h) {
        Raiz = delHoja(Raiz, papa, h);
    }
    
    private Nodo delHoja(Nodo p, int papa, int h) {
        if(p == null)
            return null;
        
        if(p.esHoja())
            return p;
        
        if(p.getData() == papa) {
            if(h < papa) {
                if(p.getHI()!=null && p.getHI().getData() == h) {
                    if(p.getHI().esHoja()) {
                        p.setHI(null);
                        return p;
                    }
                }
            } else {
                if(p.getHD()!=null && p.getHD().getData() == h) {
                    if(p.getHD().esHoja()) {
                        p.setHD(null);
                        return p;
                    }
                }
            }            
        }
        
        if(papa < p.getData())
            p.setHI( delHoja(p.getHI(), papa, h));
        else
            p.setHD( delHoja(p.getHD(), papa, h));        
        return p;
    }
    
    public int dist(int a, int b) {
        return dist(Raiz, a, b);
    }
    
    private int dist(Nodo p, int a, int b) {
        if(p == null)
            return 0;
        if(p.esHoja())
            return 0;
        
        if(p.getData() == a) {
            if(b < a)
                return distB(p.getHI(), b);
            else
                return distB(p.getHD(), b);
        }
        if(a < p.getData()) {
            int dHI = dist(p.getHI(), a, b);            
            return dHI;
        } else {
            int dHD = dist(p.getHD(), a, b);        
            return dHD;
        }
    }
    
    private int distB(Nodo p, int b) {
        if(p == null)
            return 0;
        
        if(p.esHoja()) {
            if(p.getData()==b)
                return 1;
            return 0;
        }
        
        if(p.getData() == b)
            return 1;
        
        if(b < p.getData()) {
            int dHI = distB(p.getHI(), b);
            if(dHI != 0)
                return dHI+1;            
        } else {
            int dHD = distB(p.getHD(), b);
            if(dHD != 0)
                return dHD+1;
        }
        return 0;
    }
    
    public int nivel() {
        return nivel1(Raiz);
    }
    
    private int nivel1(Nodo p) {
        if(p == null) 
            return 0;
        
        if(p.esHoja())
            return 1;
        
        int ni = nivel1(p.getHI());
        int nd = nivel1(p.getHD());
        return ni > nd ? ni + 1: nd + 1;        
    }
    
}
