/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examenes.PrimerParcial.Q_Z_2_2019.Lista;


/**
 *
 * @author Ronaldo Rivero
 */
public class Prueba {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Lista P = new Lista();
        P.add(7,3); //P = [6, 5, 7, 7, 8, 9, 6, 1, 3] El 7 se reemplaza por el primer 3.
        System.out.println(P);
        P.add(9, 2); //P = [6, 5, 7, 7, 8, 9, 6, 1, 3] El número a botar 2, no existe: La lista queda igual
        System.out.println(P);
        P.add(7, 1); // P = [6, 5, 8, 9, 6, 3] El 7 se reemplaza por el 1. Como ya hay tres 7 en la Lista, éstos se eliminan.
        System.out.println(P);
        P.add(4, 6); // P = [4, 5, 8, 9, 6, 3] El 4 se reemplaza por el primer 6.
        System.out.println(P);
        P.add(4, 6); // P = [4, 5, 8, 9, 4, 3] El 4 se reemplaza por el 6.
        System.out.println(P);
        P.add(4, 8);
        System.out.println(P);
    }
    
}
