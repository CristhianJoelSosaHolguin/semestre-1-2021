/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examenes.PrimerParcial.S_Z_1_2019.Lista;

import ADT_Lista.*;

/**
 *
 * @author Ronaldo Rivero
 */
public class Nodo {
    private int dato;
    private int peso;
    private Nodo enlace;

    public Nodo() {
        dato = -1;
        peso = -1;
        enlace = null;
    }

    public Nodo(int dato, int peso) {
        this.dato = dato;
        this.peso = peso;
        enlace = null;
    }
    
    public Nodo(Nodo nodo) {
        dato = nodo.getDato();
        peso = nodo.getPeso();
        enlace = nodo.getEnlace();
    }

    public int getDato() {
        return dato;
    }

    public void setDato(int dato) {
        this.dato = dato;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }        

    public Nodo getEnlace() {
        return enlace;
    }

    public void setEnlace(Nodo enlace) {
        this.enlace = enlace;
    }
    
    
}
