package Examenes.PrimerParcial.S_Z_1_2019.Lista;

import ADT_Lista.*;

/**
 *
 * @author Ronaldo Rivero
 */
public class Lista {
    private Nodo L;
    private int n;
    
    /**
     * Constructor de una Lista vacía
     */
    public Lista() {
        L = null;
        n = 0;
    }
    
    /**
     * Inserta el elemento X en la lista de manera ordenada con
     * el criterio ascendente
     * @param x int
     */
    public void insertar(int data, int peso) {
        if(L == null) {
            L = new Nodo(data, peso);
            n = 1;
        } else {
            Nodo azul = L;
            Nodo rojo = null;                    
            while(azul != null && azul.getDato() <= data) {                                                       
                if(azul.getDato() == data && azul.getPeso() == peso) {
                    System.err.println("No se pudo introducir "+data+" y peso "+peso+", porque ya existen");
                    return;
                }
                
                if(azul.getDato() == data) {
                    if(peso < azul.getPeso())
                        break;
                }           
                rojo = azul;
                azul = azul.getEnlace();
            }
            
            if(rojo == null) {
                Nodo aux = new Nodo(data, peso);
                aux.setEnlace(L);
                L = aux;
            } else if(rojo != null && azul != null) {
                Nodo aux = new Nodo(data, peso);
                rojo.setEnlace(aux);
                aux.setEnlace(azul);
            } else if(azul == null) {
                Nodo aux = new Nodo(data, peso);
                rojo.setEnlace(aux);
            }
            n = n + 1;
        }
    }    
    
    public int longitud() {
        return n;
    }
    
 
    /**
     * Imprimir la Lista
     * @return 
     */
    @Override
    public String toString() {
        String s = "[";
        Nodo aux = L;
        while(aux != null) {
            s = s + aux.getDato() + ", " + aux.getPeso() + " | ";
            aux = aux.getEnlace();
        }
        return s + "]";
    }
}
