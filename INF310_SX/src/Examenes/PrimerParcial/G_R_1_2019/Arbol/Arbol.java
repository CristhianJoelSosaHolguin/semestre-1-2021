package Examenes.PrimerParcial.G_R_1_2019.Arbol;

import java.util.LinkedList;

public class Arbol {
    private Nodo Raiz;
    
    public Arbol(int padre){        //Crea el árbol con raíz=padre
       Raiz = new Nodo(padre);
    }
    
    public boolean estaXNivel(int x, int nivel) {
        return estaXNivel(Raiz, x, nivel, 1);
    }
    
    private boolean estaXNivel(Nodo p, int x, int nivel, int n) {
        if(p == null)
            return false;
        if(hoja(p)) {
            if(p.getData() == x) {
                if(nivel == 0){
                    return true;
                }
                return false;
            }
        }
        
        if(p.getData() == x) {
            if(nivel == 0) {
                return true;
            }
            return false;
        }
        
        if(estaXNivel(p.getHI(), x, nivel, n+1) == true)
            return true;
        if(estaXNivel(p.getHD(), x, nivel, n+1) == true)
            return true;
        return false;
    }
    
    private Nodo delIncompleto(Nodo p, int x) {
        if(p == null)
            return null;
        if(hoja(p))
            return p;
        
        if(p.getData() == x) {
            if(p.cantHijos() == 1) {
                if(p.getHI() != null)
                    return p.getHI();
                return p.getHD();
            } else {
                return p;
            }   
        }
        
        p.setHI( delIncompleto(p.getHI(), x) );
        p.setHD( delIncompleto(p.getHD(), x) );
        return p;
    }
    
    public boolean noExisteIncompleto() {
        if(Raiz == null)
            return false;
        return noExisteIncompleto(Raiz);
    }
    
    private boolean noExisteIncompleto(Nodo p) {
        if(p == null)
            return true;
        if(hoja(p))
            return true;
        
        if(p.cantHijos() == 1)
            return false;
        
        if(noExisteIncompleto(p.getHI())==false)
            return false;
        if(noExisteIncompleto(p.getHD())==false)
            return false;
        return true;
    }
    
    public int cantIncompleto() {
        return cantIncompleto(Raiz);
    }
    
    private int cantIncompleto(Nodo p) {
        if(p == null)
            return 0;
        if(hoja(p))
            return 0;
        
        int ci = cantIncompleto(p.getHI());
        int cd = cantIncompleto(p.getHD());
        if(p.cantHijos() == 1) {
            return ci + cd + 1;
        }
        return ci + cd;
    }
    
    public void delCuasiHoja(int x){
        Raiz = delCuasiHoja(Raiz, x);
    }
    
    private Nodo delCuasiHoja(Nodo p, int x){
        if(p == null)
            return null;
        if(hoja(p)) {
            return p;
        }
        if(p.getData() == x) {
            if(p.cantHijos() == 1) {
                if(p.getHI() != null && hoja(p.getHI())) {
                    p.setData(p.getHI().getData());
                    p.setHI(null);                    
                } else if(p.getHD() != null && hoja(p.getHD())) {
                    p.setData(p.getHD().getData());
                    p.setHD(null);                    
                } 
            }
            return p;
        }
        
        p.setHI( delCuasiHoja(p.getHI(), x) );
        p.HD = delCuasiHoja(p.getHD(), x);        
        return p;
    }
    
   
   
    private boolean hoja(Nodo T){
        return (T != null && T.cantHijos() == 0);
    }
    
    
    public void add(int padre, int x){ //Inserta x al arbol, como hijo de padre. 
        Nodo p = fetch(Raiz, padre);
        if (p==null || fetch(Raiz, x) != null)
            return;     //El padre no existe o x ya está en el árbol.
       
        int i = p.getHijoNull();
        if (i != -1)
            p.setHijo(i, new Nodo(x));
    } 
    
    private Nodo fetch(Nodo t, int x){  //Fetch al nodo cuyo Data=x.
        if (t==null || t.getData() == x)
            return t;       
        
        
        Nodo hi = fetch(t.getHI(), x);
        if (hi != null)
            return hi;
        
        return fetch(t.getHD(), x);
    }
    
    public void Inorden(){
        if (Raiz == null)
            System.out.println("(Arbol vacío)");
        else{
            System.out.print("Inorden : ");
            Inorden(Raiz);
            System.out.println();
        }
    } 
     
    private void Inorden(Nodo T){
        if (T != null){
            Inorden(T.getHI());
            System.out.print(T.getData()+" ");
            Inorden(T.getHD());
        }
    }
    
    public void Preorden(){
        System.out.print("Preorden : ");
        
        if (Raiz == null)
            System.out.println("(Arbol vacío)");
        else{
            Preorden(Raiz);
            System.out.println();
        }
    }
    
    private void Preorden(Nodo T){
        if (T != null){
            System.out.print(T.getData() + " ");
            Preorden(T.getHI());
            Preorden(T.getHD());
        }
    }
    
    
    
    
    public void niveles(){ 
        System.out.print("Niveles: ");
        
        if (Raiz == null)
            System.out.println("(Arbol vacío)");
        else{
            niveles(Raiz);
        }    
    }
   
    
//---------- Métodos auxiliares para mostrar el árbol por niveles --------------
    private void niveles(Nodo T){   //Pre: T no es null.
        LinkedList <Nodo> colaNodos   = new LinkedList<>();
        LinkedList<Integer> colaNivel = new LinkedList<>();
        
        int nivelActual = 0;
        String coma="";
        
        colaNodos.addLast(T);
        colaNivel.addLast(1);
        
        do{
            Nodo p = colaNodos.pop();       //Sacar nodo de la cola
            int nivelP = colaNivel.pop();
            
            if (nivelP != nivelActual){ //Se está cambiando de nivel
                System.out.println();
                System.out.print("  Nivel "+nivelP+": ");
                nivelActual = nivelP;
                coma = "";
            }
            
            System.out.print(coma + p);
            coma = ", ";
            
            addHijos(colaNodos, colaNivel, p, nivelP);   
        }while (colaNodos.size() > 0);
        
        System.out.println();
    }
    
    private void addHijos(LinkedList <Nodo> colaNodos, LinkedList<Integer> colaNivel,  Nodo p, int nivelP){
        for (int i=1; i<=Nodo.M; i++){  //Insertar a la cola de nodos los hijos no-nulos de p
            Nodo hijo = p.getHijo(i);
            
            if (hijo != null){
                colaNodos.addLast(hijo);
                colaNivel.addLast(nivelP+1);
            }
        }
    }

}
