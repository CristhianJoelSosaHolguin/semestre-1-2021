/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_Grafo;

import java.util.ArrayList;
import java.util.List;

public class ComponenteIslas {

    private DiGrafo G;
    private boolean marcado[];
    
    public ComponenteIslas(DiGrafo G) {
        this.G = G;
        marcado = new boolean[G.cantidadDeVertice()];
    }
    
    public void mostrarComponenteDeIslas()
    {
        List<List<Integer>> islas = new ArrayList<>();
        desmarcarTodo();        
        while(estaTodoMarcado() == false) {
            int v = primerDesmarcado();
            List<Integer> isla = dfs1(v);
            islas.add(isla);
        }  
        System.out.println("Mostrando Islas");
        String s = "";
        for(int i = 0; i < islas.size(); i++) {
            s = s + "isla Nº "+i+" = ";
            for(int j = 0; j <islas.get(i).size(); j++) {
                s = s + islas.get(i).get(j)+ ", ";
            }
            s = s + "\n";
        }
        System.out.println(s);
    }    
    
    private List<Integer> dfs1(int u) {
        System.out.println(u+"");
        List<Integer> isla = new ArrayList<Integer>();
        isla.add(u);
        marcar(u);
        
        ArrayList<Integer> ady = (ArrayList<Integer>)adyacentesTotal(u);
        for(int i = 0; i < ady.size(); i++) {
            int v = ady.get(i);
            if(estaMarcado(v) == false) {
                List<Integer> subIsla = dfs1(v);
                isla.addAll(subIsla);
            }
        }
        return isla;
    }
    
    private ArrayList<Integer> adyacentesTotal(int v) {
        ArrayList<Integer> adyacentesDelVertice = (ArrayList<Integer>)G.adyacentesDeVertice(v);
        for(int i = 0; i < G.cantidadDeVertice(); i++) {
            if(i != v) {
                ArrayList<Integer> aux = (ArrayList<Integer>)G.adyacentesDeVertice(i);
                for(Integer a : aux) {
                    if(a == v) {
                        adyacentesDelVertice.add(i);
                    }
                }
            }
        }        
        return adyacentesDelVertice;
    }
    
    private int primerDesmarcado() {
        for(int i = 0; i < marcado.length; i++) {
            if(marcado[i] == false)
                return i;
        }
        return -1;
    }
    
    private boolean estaTodoMarcado() {
        for(int i = 0; i < marcado.length; i++) {
            if(marcado[i] == false)
                return false;
        }
        return true;
    }
    
    private void desmarcarTodo() {
        for(int i = 0; i < marcado.length; i++) {
            marcado[i] = false;
        }
    }
    
    private boolean estaMarcado(int v) {
        return marcado[v];
    }
    
    private void marcar(int v) {
        marcado[v] = true;
    }
}
