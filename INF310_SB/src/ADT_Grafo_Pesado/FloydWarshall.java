/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_Grafo_Pesado;

import java.util.List;

/**
 *
 * @author Ronaldo Rivero
 */
public class FloydWarshall {
    static final double INF = 63000.00;
    double[][] path;
    int[][] recorrido;
    int N;

    public FloydWarshall(Grafos G) {
        N = G.cantidadDeVertices();
        path = new double[N][N];
        recorrido = new int[N][N];
        llenarRecorrido();
        llenarInf();
        for(int i = 0; i < N; i++) {
            for(int j = 0; j < G.listaDeAdyacencia.get(i).size(); j++) {
                AdyacentesConPeso v = G.listaDeAdyacencia.get(i).get(j);
                path[i][v.getIndiceVertice()] = v.getPeso();
            }
        }
        for(int i = 0; i < N; i++) {
            path[i][i] = 0;
        }
    }
    
    public void run() {
        for(int k = 0; k < N; k++) {
            
            for(int i = 0; i < N; i++) {
                for(int j = 0; j < N; j++){
                    if(path[i][k] != INF && path[k][j] != INF) {
                        double dt = path[i][k] + path[k][j];
                        if(path[i][j] > dt){
                            path[i][j] = dt;
                            recorrido[i][j] = k;                            
                            recorrido[j][i] = k;                            
                        }
                    }
                }
            }
            
        }
    }
    
    public String mostrarMatriz() {
        String s = "   ";
        for(int k = 0; k < N; k++) {
            s = s + " ["+k+"] ";
        }
        s = s + "\n";
        for(int i = 0; i < N; i++) {
            s = s + "["+i+"]";
            for(int j = 0; j < N; j++){                    
                s = s + " " + path[i][j] + " ";
            }
            s = s + "\n";
        }
        return s;
    }
    
    public String mostrarMatrizRecorrido() {
        String s = "   ";
        for(int k = 0; k < N; k++) {
            s = s + "["+k+"]";
        }
        s = s + "\n";
        for(int i = 0; i < N; i++) {
            s = s + "["+i+"]";
            for(int j = 0; j < N; j++){                    
                s = s + " " + recorrido[i][j] + " ";
            }
            s = s + "\n";
        }
        return s;
    }
    
    public String mostrarRuta(int verticeOrigen, int verticeDestino) {                                
        double costoTotal = path[verticeOrigen][verticeDestino];
        String s = verticeOrigen+" a "+verticeDestino+" : ";
        int i = verticeOrigen;
        int j = verticeDestino;
        s = s + i + ", ";
        while(recorrido[i][j] != verticeDestino) {
            i = recorrido[i][j];
            s = s + i + ", ";
        }
        s = s + verticeDestino + " = "+costoTotal;
        return s;
    }
    
    private void llenarInf() {
        for(int i = 0; i < N; i++) {
            for(int j = 0; j < N; j++){                    
                path[i][j] = INF;
            }
        }
    }
    
    private void llenarRecorrido() {
        for(int c = 0; c < N; c++) {
            for(int f = 0; f < N; f++){                    
                recorrido[f][c] = c;
            }
        }
    }
}
