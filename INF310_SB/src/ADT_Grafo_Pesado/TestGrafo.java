/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ADT_Grafo_Pesado;

import ADT_Grafo_Pesado.excepciones.ExcepcionAristaYaExiste;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Veronica
 */
public class TestGrafo {
    
    public static void main(String argumentos[]) throws  ExcepcionAristaYaExiste{
        Prim();
        //Kruskal();
        //FloydWarshall();
    }
    
    public static void Prim() {
        try {
            Grafos g = new Grafos() ;            
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            
            g.insertarArista(0, 7, 1);
            g.insertarArista(0, 5, 3);
            g.insertarArista(1, 8, 2);
            g.insertarArista(1, 9, 3);
            g.insertarArista(1, 7, 4);
            g.insertarArista(2, 5, 4);
            g.insertarArista(3, 15, 4);
            g.insertarArista(3, 6, 5);
            g.insertarArista(4, 8, 5);
            g.insertarArista(4, 9, 6);
            g.insertarArista(5, 11, 6);
            
            System.out.println(g.mostrarGrafoLista());
            
            Prim P = new Prim(g);            
            P.run();
            System.out.println("----------- ARBOL PRIM -----------");
            System.out.println(P.mostrarArbolPrim()); 
            System.out.println("----------- GRAFO PRIM ----------");
            Grafos gp = P.getGrafoPrim();
            System.out.println(gp.mostrarGrafoLista());
            
        } catch (ADT_Grafo_Pesado.excepciones.ExcepcionAristaYaExiste ex) {
            Logger.getLogger(TestGrafo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void FloydWarshall() {
        try {
            Grafos g = new Grafos() ;            
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            
            g.insertarArista(0, 7, 1);
            g.insertarArista(0, 5, 3);
            g.insertarArista(1, 8, 2);
            g.insertarArista(1, 9, 3);
            g.insertarArista(1, 7, 4);
            g.insertarArista(2, 5, 4);
            g.insertarArista(3, 15, 4);
            g.insertarArista(3, 6, 5);
            g.insertarArista(4, 8, 5);
            g.insertarArista(4, 9, 6);
            g.insertarArista(5, 11, 6);                        
            
            System.out.println(g.mostraElGrafo());
            
            FloydWarshall F = new FloydWarshall(g);
            F.run();
            System.out.println("----------- FloydWarshall -----------");
            System.out.println(F.mostrarMatriz()); 
            System.out.println("----------- FloydWarshall Recorrido -----------");
            System.out.println(F.mostrarMatrizRecorrido());
            System.out.println(F.mostrarRuta(0, 4));
            System.out.println(F.mostrarRuta(4, 0));
        } catch (ADT_Grafo_Pesado.excepciones.ExcepcionAristaYaExiste ex) {
            Logger.getLogger(TestGrafo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void Kruskal() {
        try {
            Grafos g = new Grafos() ;            
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            g.insertarVertice();
            
            g.insertarArista(0, 7, 1);
            g.insertarArista(0, 5, 3);
            g.insertarArista(1, 8, 2);
            g.insertarArista(1, 9, 3);
            g.insertarArista(1, 7, 4);
            g.insertarArista(2, 5, 4);
            g.insertarArista(3, 15, 4);
            g.insertarArista(3, 6, 5);
            g.insertarArista(4, 8, 5);
            g.insertarArista(4, 9, 6);
            g.insertarArista(5, 11, 6);
            
            System.out.println(g.mostrarGrafoLista());
            
            Kruskal K = new Kruskal(g);
            K.run();
            System.out.println("----------- KRUSKAL -----------");
            System.out.println(K.mostrarArbol()); 
            System.out.println("----------- GRAFO KRUSKAL ----------");
            Grafos gk = K.getGrafoKruskal();
            System.out.println(gk.mostrarGrafoLista());
            
        } catch (ADT_Grafo_Pesado.excepciones.ExcepcionAristaYaExiste ex) {
            Logger.getLogger(TestGrafo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
