from ADT_Examen.Grafo import Grafo

grafo = Grafo()
grafo.agregarVertice('s.c')
grafo.agregarVertice('a.1')
grafo.agregarVertice('b.1')
grafo.agregarVertice('b.2')
grafo.agregarVertice('a.2')
grafo.agregarVertice('a.3')
grafo.agregarVertice('M.T')

grafo.agregarArista('s.c', 10, 'a.1')
grafo.agregarArista('s.c', 5, 'b.1')
grafo.agregarArista('a.1', 15, 'a.2')
grafo.agregarArista('b.1', 18, 'b.2')
grafo.agregarArista('b.2', 10, 'M.T')
grafo.agregarArista('a.2', 20, 'a.3')
grafo.agregarArista('a.3', 5, 'M.T')

grafo.imprimir()
print( grafo.alcanzable('s.c', 'M.T') )