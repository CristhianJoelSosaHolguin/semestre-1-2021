from ADT_Graph.Grafo import Grafo

grafo = Grafo()
grafo.agregarVertice()
grafo.agregarVertice()
grafo.agregarVertice()
grafo.agregarVertice()
grafo.agregarVertice()

grafo.agregarArista(0, 20, 2)
grafo.agregarArista(1, 30, 2)
grafo.agregarArista(1, 2, 0)
grafo.agregarArista(2, 10, 3)
grafo.agregarArista(2, 10, 4)
grafo.agregarArista(3, 15, 1)

grafo.imprimir()
grafo.dfs(0)