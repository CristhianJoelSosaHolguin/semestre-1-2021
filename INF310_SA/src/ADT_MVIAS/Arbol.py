# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
from Nodo import Nodo

class Arbol:
    
    def __init__(self, M):
        self.__M = M
        self.__raiz = None
        
    def __existe(self, dato, p):
        if p == None:
            return False
        else:
            if p.existe(dato):
                return True
         
        ph = p.getPosHijo(dato)
        return  self.__existe( dato, p.getHijo( ph ) )
    
    def insertar(self, dato):
        if not self.__existe(dato, self.__raiz):
            self.__raiz = self.__insertar1(dato, self.__raiz)
        else:
            print('El dato ya existe')

    def __insertar1(self, dato, p):
        if p == None:
            return Nodo(self.__M, dato)
        else:
            if not p.lleno():
                p.insertar(dato)
                return p
            else:
                ph = p.getPosHijo(dato)
                p.setHijo( self.__insertar1(dato, p.getHijo(ph))  ,  ph )
            return p
    
    def inOrden(self):
        self.__inOrden1(self.__raiz)

    def __inOrden1(self, p):
        if p != None:
            for i in range(p.cantDatos()):
                self.__inOrden1(p.getHijo( i ))
                print(' - ', p.getData( i ))
            self.__inOrden1(  p.getHijo( p.cantDatos() ) )
    
    def sumaNodo(self):
        return self.__sumaNodo1(self.__raiz)
    
    def __sumaNodo1(self, p):
        if p is None:
            return 0
        if p.esHoja():
            s = 0
            for i in range(p.cantDatos()):                
                s = s + p.getData(i)
            return s

        s = 0
        for i in range(self.__M):
            s = s + self.__sumaNodo1(p.getHijo(i))
        for i in range(p.cantDatos()):        
            s = s + p.getData(i)   
        return s
    
    def cantNodos(self):
        return self.__cantNodos1(self.__raiz)
    
    def __cantNodos1(self, p):
        if p is None:
            return 0
        if p.esHoja():
            return 1
        cn = 0
        for i in range(self.__M):
            cn = cn + self.__cantNodos1( p.getHijo(i) )
        return cn+1
        
    def cantNodosLlenos(self):
        return self.__cantNodosLlenos1(self.__raiz)
    
    def __cantNodosLlenos1(self, p):
        if p is None:
            return 0
        if p.esHoja():
            if p.lleno():
                return 1
            return 0
        
        cnl = 0
        for i in range(self.__M):
            cnl = cnl + self.__cantNodosLlenos1( p.getHijo(i) )
        
        if p.lleno():
            cnl = cnl + 1
        return cnl
    
    def cantNodosEscalera(self):
        return self.__cantNodosEscalera1(self.__raiz)
    
    def __cantNodosEscalera1(self, p):
        if p is None:
            return 0
        if p.esHoja():
            if self.__datosEscalera(p):
                return 1
            return 0
        
        cne = 0
        for i in range(self.__M):
            cne = cne + self.__cantNodosEscalera1( p.getHijo(i) )
        
        if self.__datosEscalera(p):
            cne = cne + 1
        return cne
            
    def __datosEscalera(self, p):
        if p.lleno() == False:
            return False
        d = p.getData(0)
        for i in range(1, p.cantDatos()):
            d = d + 1
            if d != p.getData(i):
                return False                 
        return True
        